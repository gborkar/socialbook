import './style.css';

function Main() {
  var settingsmenu = document.querySelector(".settings-menu");
  function settingsMenuToggle() {
    settingsmenu.classList.toggle("settings-menu-height");
  }
  return (
    <div>
        <nav>
          <div className="nav-left">
            <a href="Index.html">
              <img
                src="https://i.postimg.cc/Y9nZymQq/logo2.png" alt="something" 
                className="logo"
              />
            </a>
            <ul>
              <li>
                <img src="https://i.postimg.cc/Fs3m1Djy/notification.png" alt="something" />
              </li>
              <li>
                <img src="https://i.postimg.cc/YqGKZ8nc/inbox.png" alt="something" />
              </li>
              <li>
                <img src="https://i.postimg.cc/xCzpgFjg/video.png" alt="something" />
              </li>
            </ul>
          </div>
          <div className="nav-right">
            <div className="search-box">
              <img src="https://i.postimg.cc/SKtHgM6Q/search.png" alt='Ok' />
              <input type="text" placeholder="Search" />
            </div>
            <div
              className="nav-user-icon online"
              onClick={settingsMenuToggle}
            >
              <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
            </div>
          </div>
          {/*--------------Settings Menu"---------------------*/}
          <div className="settings-menu">
            <div id="dark-btn">
              <span />
            </div>
            <div className="settings-menu-inner">
              <div className="user-profile">
                <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                <div>
                  <p>John Nicholson</p>
                  <a href="profile.html">See your profile</a>
                </div>
              </div>
              <hr />
              <div className="user-profile">
                <img src="https://i.postimg.cc/hv3nx52s/feedback.png" alt="description"/>
                <div>
                  <p>Give Feedback</p>
                  <a href="# ">Help us to improve the new design</a>
                </div>
              </div>
              <hr />
              <div className="settings-links">
                <img
                  src="https://i.postimg.cc/QCcPNYRV/setting.png" alt="something" 
                  className="settings-icon"
                />
                <a href="# ">
                  Settings &amp; Privacy{" "}
                  <img
                    src="https://i.postimg.cc/RF1dBMWr/arrow.png" alt="something" 
                    width="10px"
                  />
                </a>
              </div>
              <div className="settings-links">
                <img
                  src="https://i.postimg.cc/C5tydfK6/help.png" alt="something" 
                  className="settings-icon"
                />
                <a href="# ">
                  Help &amp; Support
                  <img
                    src="https://i.postimg.cc/RF1dBMWr/arrow.png" alt="something" 
                    width="10px"
                  />
                </a>
              </div>
              <div className="settings-links">
                <img
                  src="https://i.postimg.cc/5yt1XVSj/display.png" alt="something" 
                  className="settings-icon"
                />
                <a href="# ">
                  Display &amp; Accessibility{" "}
                  <img
                    src="https://i.postimg.cc/RF1dBMWr/arrow.png" alt="something" 
                    width="10px"
                  />
                </a>
              </div>
              <div className="settings-links">
                <img
                  src="https://i.postimg.cc/PJC9GrMb/logout.png" alt="something" 
                  className="settings-icon"
                />
                <a href="# ">
                  Logout{" "}
                  <img
                    src="https://i.postimg.cc/RF1dBMWr/arrow.png" alt="something" 
                    width="10px"
                  />
                </a>
              </div>
            </div>
          </div>
        </nav>
        <div className="container">
          {/*--------------Left Sidebar---------------------*/}
          <div className="left-sidebar">
            <div className="imp-links">
              <a href="#  ">
                <img src="https://i.postimg.cc/RCj4MjnC/news.png" alt="something" />
                Latest News
              </a>
              <a href="# ">
                <img src="https://i.postimg.cc/MpBwMtV0/friends.png" alt="something" />
                Friendss
              </a>
              <a href="# ">
                <img src="https://i.postimg.cc/44FRWj1b/group.png" alt="something" />
                Group
              </a>
              <a href="# ">
                <img src="https://i.postimg.cc/0jh39RtT/marketplace.png" alt="something" />
                Marketplace
              </a>
              <a href="# ">
                <img src="https://i.postimg.cc/VsXHCTVk/watch.png" alt="something" />
                Watch
              </a>
              <a href="# ">See More</a>
            </div>
            <div className="shortcut-link">
              <p>Your Shortcuts</p>
              <a href="# ">
                <img src="https://i.postimg.cc/3JHVf7vG/shortcut-1.png" alt="something" />
                Web Developers
              </a>
              <a href="# ">
                <img src="https://i.postimg.cc/rFCbvb1P/shortcut-2.png" alt="something" />
                Web Design course
              </a>
              <a href="# ">
                <img src="https://i.postimg.cc/0yk3xfZ2/shortcut-3.png" alt="something" />
                Full Strack Development
              </a>
              <a href="# ">
                <img src="https://i.postimg.cc/Z5wQqdDP/shortcut-4.png" alt="something" />
                Website Experts
              </a>
            </div>
          </div>
          {/*--------------Main Sidebar---------------------*/}
          <div className="main-content">
            <div className="story-gallery">
              <div className="story story1">
                <img src="https://i.postimg.cc/TPh453Zz/upload.png" alt="something" />
                <p>Post Story</p>
              </div>
              <div className="story story2">
                <img src="https://i.postimg.cc/XNPtfdVs/member-1.png" alt="something" />
                <p>Alison</p>
              </div>
              <div className="story story3">
                <img src="https://i.postimg.cc/4NhqByys/member-2.png" alt="something" />
                <p>Jackson</p>
              </div>
              <div className="story story4">
                <img src="https://i.postimg.cc/FH5qqvkc/member-3.png" alt="something" />
                <p>Samona</p>
              </div>
              <div className="story story5">
                <img src="https://i.postimg.cc/Sx65bPcP/member-4.png" alt="something" />
                <p>John Doe</p>
              </div>
            </div>
            <div className="write-post-container">
              <div className="user-profile">
                <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                <div>
                  <p>John Nicholson</p>
                  <small>
                    Public <i className="fas fa-caret-down" />
                  </small>
                </div>
              </div>
              <div className="post-input-container">
                <textarea
                  rows={3}
                  placeholder="What's on your mind, John?"
                  defaultValue={""}
                />
                <div className="add-post-links">
                  <a href="# ">
                    <img src="https://i.postimg.cc/QMD2BDXs/live-video.png" alt="something" />
                    Live Video
                  </a>
                  <a href="# ">
                    <img src="https://i.postimg.cc/6pKKZn0D/photo.png" alt="something" />
                    Photo/Video
                  </a>
                  <a href="# ">
                    <img src="https://i.postimg.cc/Pf6TBCdD/feeling.png" alt="description"/>
                    Feling/Activity
                  </a>
                </div>
              </div>
            </div>
            <div className="post-container">
              <div className="post-row">
                <div className="user-profile">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <div>
                    <p>John Nicholson</p>
                    <span>June 24 2021, 13:40 pm</span>
                  </div>
                </div>
                <a href="# ">
                  <i className="fas fa-ellipsis-v" />
                </a>
              </div>
              <p className="post-text">
                Subscribe <span>@Vkive Tutorials</span> Youtube Channel to watch
                more videos on website development and UI desings.{" "}
                <a href="# "># VkiveTutorials</a> <a href="# "># YoutubeChannel</a>
              </p>
              <img
                src="https://i.postimg.cc/9fjhGTY6/feed-image-1.png" alt="something" 
                className="post-img"
              />
              <div className="post-row">
                <div className="activity-icons">
                  <div>
                    <img src="https://i.postimg.cc/pLKNXrMq/like-blue.png" alt="something" />
                    120
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/rmjMymWv/comments.png" alt="something" />
                    45
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/T2bBchpG/share.png" alt="something" />
                    20
                  </div>
                </div>
                <div className="post-porfile-icon">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <i className="fas fa-caret-down" />
                </div>
              </div>
            </div>
            <div className="post-container">
              <div className="post-row">
                <div className="user-profile">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <div>
                    <p>John Nicholson</p>
                    <span>June 24 2021, 13:40 pm</span>
                  </div>
                </div>
                <a href="# ">
                  <i className="fas fa-ellipsis-v" />
                </a>
              </div>
              <p className="post-text">
                Like and share this video with friends, tag
                <span>@Vkive Tutorials</span>facebook page on your post. Ask
                your dobuts in the comments. <a href="# "># VkiveTutorials</a>{" "}
                <a href="# "># YoutubeChannel</a>
              </p>
              <img
                src="https://i.postimg.cc/Xvc0xJ2p/feed-image-2.png" alt="something" 
                className="post-img"
              />
              <div className="post-row">
                <div className="activity-icons">
                  <div>
                    <img src="https://i.postimg.cc/pLKNXrMq/like-blue.png" alt="something" />
                    120
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/rmjMymWv/comments.png" alt="something" />
                    45
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/T2bBchpG/share.png" alt="something" />
                    20
                  </div>
                </div>
                <div className="post-porfile-icon">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <i className="fas fa-caret-down" />
                </div>
              </div>
            </div>
            <div className="post-container">
              <div className="post-row">
                <div className="user-profile">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <div>
                    <p>John Nicholson</p>
                    <span>June 24 2021, 13:40 pm</span>
                  </div>
                </div>
                <a href="# ">
                  <i className="fas fa-ellipsis-v" />
                </a>
              </div>
              <p className="post-text">
                Like and share this video with friends, tag
                <span>@Vkive Tutorials</span>facebook page on your post. Ask
                your dobuts in the comments. <a href="# "># VkiveTutorials</a>{" "}
                <a href="# "># YoutubeChannel</a>
              </p>
              <img
                src="https://i.postimg.cc/tJ7QXz9x/feed-image-3.png" alt="something" 
                className="post-img"
              />
              <div className="post-row">
                <div className="activity-icons">
                  <div>
                    <img src="https://i.postimg.cc/pLKNXrMq/like-blue.png" alt="something" />
                    120
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/rmjMymWv/comments.png" alt="description"/>
                    45
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/T2bBchpG/share.png" alt="something" />
                    20
                  </div>
                </div>
                <div className="post-porfile-icon">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="description"/>
                  <i className="fas fa-caret-down" />
                </div>
              </div>
            </div>
            <div className="post-container">
              <div className="post-row">
                <div className="user-profile">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="description"/>
                  <div>
                    <p>John Nicholson</p>
                    <span>June 24 2021, 13:40 pm</span>
                  </div>
                </div>
                <a href="# ">
                  <i className="fas fa-ellipsis-v" />
                </a>
              </div>
              <p className="post-text">
                Like and share this video with friends, tag
                <span>@Vkive Tutorials</span>facebook page on your post. Ask
                your dobuts in the comments. <a href="# "># VkiveTutorials</a>{" "}
                <a href="# "># YoutubeChannel</a>
              </p>
              <img
                src="https://i.postimg.cc/hjDRYBwM/feed-image-4.png" alt="description"
                className="post-img"
              />
              <div className="post-row">
                <div className="activity-icons">
                  <div>
                    <img src="https://i.postimg.cc/pLKNXrMq/like-blue.png" alt="something" />
                    120
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/rmjMymWv/comments.png" alt="description"/>
                    45
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/T2bBchpG/share.png" alt="description"/>
                    20
                  </div>
                </div>
                <div className="post-porfile-icon">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <i className="fas fa-caret-down" />
                </div>
              </div>
            </div>
            <div className="post-container">
              <div className="post-row">
                <div className="user-profile">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <div>
                    <p>John Nicholson</p>
                    <span>June 24 2021, 13:40 pm</span>
                  </div>
                </div>
                <a href="# ">
                  <i className="fas fa-ellipsis-v" />
                </a>
              </div>
              <p className="post-text">
                Like and share this video with friends, tag
                <span>@Vkive Tutorials</span>facebook page on your post. Ask
                your dobuts in the comments. <a href="# "># VkiveTutorials</a>{" "}
                <a href="# "># YoutubeChannel</a>
              </p>
              <img
                src="https://i.postimg.cc/ZRwztQzm/feed-image-5.png" alt="something" 
                className="post-img"
              />
              <div className="post-row">
                <div className="activity-icons">
                  <div>
                    <img src="https://i.postimg.cc/pLKNXrMq/like-blue.png" alt="something" />
                    120
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/rmjMymWv/comments.png" alt="something" />
                    45
                  </div>
                  <div>
                    <img src="https://i.postimg.cc/T2bBchpG/share.png" alt="something" />
                    20
                  </div>
                </div>
                <div className="post-porfile-icon">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="something" />
                  <i className="fas fa-caret-down" />
                </div>
              </div>
            </div>
            <button type="button" className="load-more-btn">
              Load More
            </button>
          </div>
          {/*--------------Right Sidebar---------------------*/}
          <div className="right-sidebar">
            <div className="sidebar-title">
              <h4>Events</h4>
              <a href="# ">See All</a>
            </div>
            <div className="event">
              <div className="left-event">
                <h3>18</h3>
                <span>March</span>
              </div>
              <div className="right-event">
                <h4>Social Media</h4>
                <p>
                  <i className="fas fa-map-marker-alt" /> Willson Tech Park
                </p>
                <a href="# ">More Info</a>
              </div>
            </div>
            <div className="event">
              <div className="left-event">
                <h3>22</h3>
                <span>June</span>
              </div>
              <div className="right-event">
                <h4>Mobile Marketing</h4>
                <p>
                  <i className="fas fa-map-marker-alt" /> Willson Tech Park
                </p>
                <a href="# ">More Info</a>
              </div>
            </div>
            <div className="sidebar-title">
              <h4>Advertisement</h4>
              <a href="# ">close</a>
            </div>
            <img
              src="https://i.postimg.cc/CLXYx9BL/advertisement.png" alt="something" 
              className="siderbar-ads"
            />
            <div className="sidebar-title">
              <h4>Conversation</h4>
              <a href="# ">Hide Chat</a>
            </div>
            <div className="online-list">
              <div className="online">
                <img src="https://i.postimg.cc/XNPtfdVs/member-1.png" alt="something" />
              </div>
              <p>Alison Mina</p>
            </div>
            <div className="online-list">
              <div className="online">
                <img src="https://i.postimg.cc/4NhqByys/member-2.png" alt="something"  />
              </div>
              <p>Jackson Aston</p>
            </div>
            <div className="online-list">
              <div className="online">
                <img src="https://i.postimg.cc/FH5qqvkc/member-3.png" alt="something" />
              </div>
              <p>Samona Rose</p>
            </div>
            <div className="online-list">
              <div className="online">
                <img src="https://i.postimg.cc/Sx65bPcP/member-4.png" alt="something" />
              </div>
              <p>Mike Pérez</p>
            </div>
          </div>
        </div>
      <p />
    </div>
  );
}

export default Main;
